---
title: "Heat pump detection from open and smart meter data"
output: html_notebook
---



```{r Load packages and data}
library(ResidentialEnergyConsumption) # Data package with smart meter, weather, and ground truth data etc.
library(SmartMeterAnalytics) # Calculates smart meter features etc.
library(tidyverse) # Important package to use in everyday data analyses.
library(caret) # Machine learning
library(FSelector) # for feature selection / you need Java installed to load this package
library(pROC) # Plot ROC curves
```


```{r Prepare datasets}
data(elcons_15min)
View(elcons_15min)

data(heatinginfo_15min)
View(heatinginfo_15min)

data(weather_data)
View(weather_data)

# Extract smart meter features
# Select a single exemplary week. e.g. w44
week44 <- elcons_15min[["w44"]]
View(week44)

smd_features <- suppressWarnings(apply(week44[,-1], 1, calc_features15_consumption))
smd_features <- dplyr::bind_rows(smd_features)
smd_features$VID <- week44[,1]
smd_features <- smd_features %>% select(VID, everything()) 


# Prepare prediction variable
heatinginfo_15min <- heatinginfo_15min %>%
  mutate(heatpump = case_when(heating_type == "heat pump" ~ "heatpump",
                              TRUE ~ "other"))



# Integrate data
data_integrated <- heatinginfo_15min %>%
  select(VID, heatpump) %>%
  left_join(smd_features, by = "VID") %>%
  mutate(heatpump = as_factor(heatpump)) %>%
  drop_na()
  

```

```{r Build models}

set.seed(107)
inTrain <- createDataPartition(
  y = data_integrated$heatpump,
  ## the outcome data are needed
  p = .75,
  ## The percentage of data in the
  ## training set
  list = FALSE
)

training <- data_integrated[ inTrain,]
testing  <- data_integrated[-inTrain,]

selected_features <- cfs(heatpump ~ ., data_integrated)

fitControl <- trainControl(## 10-fold CV
                           method = "repeatedcv",
                           number = 10,
                           ## repeated ten times
                           repeats = 10,
                           classProbs = T,
                           savePredictions = T)

model <- train(
  as.simple.formula(selected_features, "heatpump"),
  data = training,
  method = "rf",
  trControl = fitControl,
  ## Center and scale the predictors for the training
  ## set and all future samples.
  preProc = c("center", "scale")
)

```


```{r Evaluate models}
model
confusionMatrix(model)

# Select a parameter setting
selectedIndices <- model$pred$mtry == 2
# Plot:
plot.roc(model$pred$obs[selectedIndices],
         model$pred$heatpump[selectedIndices])

predicted <- predict(model, newdata = testing, type = "prob")
head(predicted)
predicted <- predict(model, newdata = testing)

confusionMatrix(data = predicted, testing$heatpump)
```


